# JDBC

**JDBC**: Java Database Connectivity is an application programming interface for the programming language Java, which defines how a client may access a database. It is a Java-based data access technology used for Java database connectivity.

- Developed by the Oracle Corporation
- Provides methods to query and update a database 
- Focuses on relational databases
- Connection to specific databases enabled through drivers
JDBC defines the objects and methods needed by the application to enable this communication.

## JDBC Objects

- **DataSource and DriverManager**: Establish the connection to the database
- **Connection**: Manages the connection
- **Statement objects**: *Statement, PreparedStatement, CallableStatement* - used to execute SQL queries
- **ResultSet**: Contains the results of a query execution performed using a Statement.

### Statements

- **Statement**: one-off SQL statements
- **PreparedStatement**: Parametrized statement for frequent use
- **CallableStatement**: Parametrized - used for stored procedures

#### PreparedStatement

- A pre-compiled SQL statement
- Used to execute similar statements repeatedly
- Statement is parsed, optimized, and translated just once. This overhead need not be incurred for each execution
- Each execution only requires parameters to be plugged in

## ResultSet

A ResultSet is a tabular representation of query results
- It maintains a cursor pointing a **current** row
- The cursor can be used to read values from specific columns
- A ResultSet can be used to update data
- Rows can be deleted
- New rows can be added

### Properties of ResultSet

- Remains connected to the database until closed
- By default, can only move forward and is read-only
- Can be made scrollable and updatable (driver-specific)
- May be sensitive to changes in underlying data (driver-specific)

## JDBC RowSet

A RowSet is an extension of a ResultSet and offers several additional features:
- **JavaBeans Component**: Includes properties and a notification mechanism
- **Scrollable and updatable**: Especially useful if the driver does not support a ResultSet with these properties
- **Available in multiple types**: JdbcRowSet, CachedRowSet, WebRowSet, JoinRowSet, and FilteredRowSet
- **Can be disconnected from the database**: Requires a connection only when being populated and to push updates.

### RowSet Types

- Connected RowSet: Can pick up updates to data after it has been populated
  - The JdbcRowSet
    - Wrapper around a ResultSet: Can set up a connection, execute a statement, and capture results
    - Many implementations: Is an interface in `javax.sql.rowset`
- Disconnected RowSet
  - Uses a connection to populate itself
  - Can manipulate data while disconnected
  - Reconnects to write changes to DB

#### Type of Disconnected RowSets

- CachedRowSet : Can manipulate data while disconnected
- WebRowSet - CachedRowSet that can be represented as an XML document
- JoinRowSet - a WebRowSet that can do a SQL JOIN using disconnected RowSets
- FilteredRowSet - a WebRowSet to which filtering criteria (effectively a WHERE clause) can be applied

## Running Multiple Statements

### Executing Statements

Statements are executed over connection objects. What if the application needs to perform several updates?

#### Batch Updates

- Greatly reduce network overhead
- Database may run some updates in parallel
- Batches can be created for statement and PreparedStatement instances
- If there are errors in an execution, a batch may only be partially executed - transactions may be needed for atomic updates

##### Transactions

- All-or-nothing execution: Required for crucial operations like money transfers
- Rollback: If there is an error in the middle of the transaction, changes can be undone
- Savepoint: Checkpoints to roll back to - may avoid having to undo everything
- Disable auto-commit: Enabled by default in many cases, perform explicit commit at the end of the transaction.



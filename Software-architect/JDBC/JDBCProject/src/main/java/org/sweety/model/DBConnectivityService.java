package org.sweety.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnectivityService {

    private static Logger LOGGER = Logger.getLogger(DBConnectivityService.class.getName());
    private String databaseUrl = "jdbc:mysql://localhost:3306";
    private String user = "root";
    private String password = "root";
    Connection connection = null;

    public void getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(databaseUrl, user, password);
            if (Objects.nonNull(connection)){
                LOGGER.log(Level.INFO,"The connection has been successfully established!");

            }
        }catch (SQLException e) {
            LOGGER.log(Level.SEVERE,"A connection error has occurred: ", e);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Driver not found: ", e);
        }
    }
}

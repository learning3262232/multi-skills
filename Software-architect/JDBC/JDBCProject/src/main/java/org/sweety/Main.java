package org.sweety;

import org.sweety.model.DBConnectivityService;

public class Main {
    public static void main(String[] args) {
        DBConnectivityService dbConnectivityService = new DBConnectivityService();
        dbConnectivityService.getConnection();
    }
}
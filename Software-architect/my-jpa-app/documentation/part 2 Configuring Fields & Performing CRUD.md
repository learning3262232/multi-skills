# Configuring Fields & Performing CRUD Operations

- *@Table* name table into database
- *@Id*: The primary Key mandatory
- *@GeneratedValue*: Generate unique identifier for primary keys.
Generation strategy **AUTO (default strategy)**: The JPA provider chooses the strategy, Hiernate uses a database sequence.

## Generation Types AuTO and IDENTITY

- **GenerationType.Auto**:  JPA lets the JPA provider, which in our case is Hibernate to choose the strategy for generating primary keys. Hibernate chooses the sequence generation option.
- **GenerationType.IDENTITY**:  This generation strategy is by far the easiest one to use, it does not require the setup of a separate database sequence.

*Notes*: **Generation strategy IDENTITY**: relies on an auto-incremented database column
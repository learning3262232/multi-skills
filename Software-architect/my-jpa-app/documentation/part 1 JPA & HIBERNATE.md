# JPA & HIBERNATE

## Overview
JP or Java Persistence API is focused on persistence. Persistence can refer to any mechanism by which Java objects outlive the applications that create them.
It defines a set of concepts that can be implemented by any tool or framework. JP supports object relational mapping, a mapping from objects in high level.

NB: hibernate implement JPA

## Object-relational Mapping

### Databases for Structured Data Storage

- Databases provide persistent storage and ease-of-access
- Use Structured Query Language (SQL) to access data
- SQL is closely associated with relational databases
- Even NoSQL databases support SQL-like languages.

### Programmatic Access

- SQL is great for interactive operations by human user
- Not always most convenient for programmatic access
- All meaningful application programs have state
- This state is often best stored in a database

#### Paradigms for programmatic database access

- Open Database Connectivity (ODBC): is a standard application programming interface for accessing database management systems. The designers of ODBC aim to make it independent of database systems and operating systems.
  - ODBC serves as a standard API for programmatic DBMS access
  - Databases can be changed without changing programs
  - ODBC drivers in programming languages
  - Connect to database, run queries, commands...
- Java Database Connectivity (JDBC): is a standard API written in Java allowing you to access underlying relational databases from Java code.
  - Standard Java API for database connectivity similar in concept to ODBC
  - Connections, queries, transactions, commands like ODBC, JDBC is best for relational databases

### Relational Databases vs. Java Classes

![](../captures\databasevsjavaclass.png "Database vs Java classes")

#### Object-Relational "Impedance Mismatch"

![](../captures\databasevsjavaclassemodel.png "Impedance Mismatch")

#### Object-relational Mapping (ORM)
![](../captures\orm.png)

#### ORMs for Programmatic Access from Java

- Programs freed from mapping from objects to relations
- Based on JDBC under-the-hood
- ORM framework use JDBC to manage connection between objects to databases, manage transactions, queries, access...
- Cope with inheritance, object equality, and associations
- Transaction management and primary key generation
- ORM understand database constructs as well as object-oriented.

#### ORMs Frameworks

- Hibernate
- Castor
- Java Data Objects (JDO)
- Enterprise JavaBeans, Entity Beans
- Spring DAO

## JPA & Hibernate

*JPA is the Java Persistence API, and Hibernate framework that implements the JPA model.*

### JPA

- **Specification for persistence providers implemented by ORMs**
- Specification to standardize ORM behavior
- Was initially heavily inspired by Hibernate
- Hibernate ORM: mature JPA implementation
- Hibernate OGM: Hibernate NoSQL JPA
- Eclipselink: alternative implementation of JPA

![](../captures\archiJPA.png "JPA Architecture")

#### JPA Annotations

- JPA provides metadata annotations that can be applied to classes, member variables of classes, getters, setters, constructors...
- These annotations map from Java to DBMS help map from Java code to the underlying Database Management System
- Each JPA implementation supports these.
- JPA spec also provides two additional classes (PersistenceManager & Entity Manager). These classes contain the business logic to manage entities and their persistence life cycle.
![](../captures\jparelationship.png "Class Relationship in JPA")

#### JPA Mappings

- ORM: Object-relational Mapping
- Which object maps to which database table
- JPA implementation has mapping engine.
- Use of mappings simplies the creation of the objects
- No need for persistence logic inside object
- Mappings are a key JPA concept
- By default, name of object = name of table
- Fields become columns
- Each table row corresponds to an object
- Primary keys used to identify objects
- CRUD operations supported on objects
- Different types of entity relationships

#### Entity Relationships in JPA

- One-to-many
- Many-to-one
- Many-to-many
- One-to-one
### Hibernate

- Important java Persistence framework that implements JPA (and served as basis for initial spec of JPA)
- ORM framework and more
- Hibernate Search is a popular extension
- Hibernate OGM for NoSQL access
- Hibernate Validator for constraints
- **Hibernate implements JPA spec**
- JPA annotations Hibernate implementation
NB: Hibernate offers the implementation, JPA offer the API.

### Reasons for the Popularity of Hibernate

- Ease-of-use
  - No specialized databases or fields required
  - Any class can be persisted using Hibernate
  - Support for polymorphism and inheritance
  - Transaction and connection management
  - Automatic primary key generation
  - Named queries
- Performance and Scalability
  - Lazy initialization aka & lazy loading
  - Multiple levels of caching
  - First-level caching within your hibernate session
  - Second-level caching that works across Hibernate sessions
  - Different caching providers and strategies
  - Optimistic locking, you can lock your database table before you perform operations on it.
- HQL (Hibernate Query Language)
  - Object-oriented query language
  - As similar to SQL as possible
  - SuperSet of Java Persistent Query Language (JPQL)
  - Allows mappings from Java to database
  - Mappings can be defined via annotation or in other ways too

![](../captures\archHibernate.png "Hibernate Architecture")



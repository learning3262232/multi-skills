package org.yvonz.jpa.models;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class BookRepository implements DBService{
    private EntityManagerFactory factory;
    private EntityManager entityManager;

    public BookRepository() {
        this.factory =  Persistence.createEntityManagerFactory("BookstoreDB_Unit");
        this.entityManager = factory.createEntityManager();
    }
    @Override
    public void create(Book book) {
        entityManager.getTransaction().begin();
        entityManager.persist(book);
        entityManager.getTransaction().commit();
        this.close();
    }

    @Override
    public boolean edit(Book book) {
        entityManager.getTransaction().begin();
        return false;
    }

    private void close() {
        entityManager.close();
        factory.close();
    }

    public static  DBService getInstance(){
        DBService service = new BookRepository();
        return service;
    }
}

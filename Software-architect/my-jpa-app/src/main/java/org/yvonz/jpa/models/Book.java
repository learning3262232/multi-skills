package org.yvonz.jpa.models;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author herve.yvon.f.zobo
 * @version {@value }
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "and")
@Entity //persistence domain object
@Table(name = "books") // name table into database
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String title;
    private String author;
    private String description;
    private BigDecimal price;
    private LocalDateTime createdAt;
}

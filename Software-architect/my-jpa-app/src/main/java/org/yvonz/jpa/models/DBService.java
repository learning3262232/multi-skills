package org.yvonz.jpa.models;

public interface DBService {
    void create(Book book);
    boolean edit(Book book);
}

package org.yvonz.jpa;

import org.yvonz.jpa.models.Book;
import org.yvonz.jpa.models.BookRepository;
import org.yvonz.jpa.models.DBService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class App {

    public static void main(String... args){
        Book book = Book.builder()
                //.andId(UUID.randomUUID().toString())
                .andAuthor("Yvon ZOBO")
                .andTitle("The World War, Tom 3")
                .andDescription("Why our world is like that!")
                .andPrice(BigDecimal.valueOf(300.0))
                .andCreatedAt(LocalDateTime.now())
                .build();
        BookRepository.getInstance().create(book);
        Book book1 = Book.builder()
               // .andId(UUID.randomUUID().toString())
                .andAuthor("Yvon ZOBO")
                .andTitle("The World War, Tom 2")
                .andDescription("Why our world is like that!")
                .andPrice(BigDecimal.valueOf(400.0))
                .andCreatedAt(LocalDateTime.now())
                .build();
        BookRepository.getInstance().create(book1);
        Book book3 = Book.builder()
                //.andId(UUID.randomUUID().toString())
                .andAuthor("Yvon ZOBO")
                .andTitle("The World War, Tom 4")
                .andDescription("Why our world is like that!")
                .andPrice(BigDecimal.valueOf(350.0))
                .andCreatedAt(LocalDateTime.now())
                .build();
        BookRepository.getInstance().create(book3);
    }
}

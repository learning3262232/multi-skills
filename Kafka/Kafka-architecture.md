# Kafka Architecture

## Publisher/Subscriber Messaging in KAFKA

### Topics

A **Topic** is a data feed to which data records are published and from which they can be consumed.

Publishers send data to a topic, and subscribers read data from the topic. This is known as **publisher/Subscriber**, or simply **pub/sub** for short.

### The Topic Log

Kafka topics each maintain a log.

The **log** is an ordered, immutable list of data records.

The log for a Kafka topic is usually divided into multiple **partitions**. This is what allows Kafka to process data efficiently and in a scalable fashion.
Each record in a partition has a sequential, unique ID called an **offset**.

### Producers

A **Producer** is the publisher in the pub/sub model. The producer is simply an application that communicates with the cluster, and can be in a separate process or on a different server.

Producers write data records to the Kafka topic.

For each new record, the Producer determines which partition to write to, often in a simple round-robin fashion. You can customize this to use a more sophisticated algorithm for determining which topic to write a new record to.
![](captures\producer.png "Kafka Producers")

### Consumers

A **Consumer** is the subscriber in the pub/sub model. Like producers, consumers are external applications that can be in a separate process or on a different server from Kafka itself.

Consumers read data from Kafka topics. Each consumer controls the offset it is currently reading for each partition, and consumers normally read records in order based on the offset.

You can have any number of consumers for a topic, and they can all process the same records.

Recorsd are not deleted when they are consumed. They are only deleted based upon a configurable retention period.
![](captures\consumer.png "Kafka Consumer")

### Consumer Groups

By Default, all consumers will process all records, but what if you want to scale your record processing so that multiple instances can process the data without two instances processing the same record?

You can place consumers into **Consumer Groups**. Each record will be consumed by exactly one consumer per consumer group.

With consumer groups, Kafka dynamically assigns each partition to exactly one consumer in the groups. If you have more consumer than partitions, some of the consumers will be idle and will not process records.

![](captures\consumerGroups.png "Consumer Groups")

### Terms

- **Topic**: A named data feed that data can be written to  and read from.
- **Log**: The data structure used to store a topic's data. The log is a partitioned, immutable sequence of data records.
- **Partition**: A section of a topic's log.
- **Offset**: The sequential and unique ID of a data record within a partition.
- **Producer**: Something that writes data to a topic.
- **Consumer**: something that reads data from a topic.
- **Consumer Group**: A group of multiple consumers. Normally, multiple consumers can all consume the same record from a topic, but only one consumer in a consumer group will consume reach record.


## Kafka Architecture

### Brokers

*The central component of Kafka architecture is the broker*.

**Brokers** are the servers that make up a **Kafka cluster** (one or more brokers).

Producers and consumers communicate with brokers in order to publish and consumer messages.

### Zookeeper

Kafka depends on an underlying technology called Zookeeper.

**Zookeeper** is a generalized cluster management tool. It manages the cluster and provides a consistent, distributed place to store cluster configuration.

Zookeeper coordinates communication throughout the cluster, adds and removes brokers, and monitors the status of nodes in the cluster. It is often installed alongside Kafka, but can be maintained on a completely separate set of servers.
![](C:\Users\herve.yvon.f.zobo\apps\multi-skills\Kafka\captures\kafka_cluster.png "Kafka Cluster")

### Networking

Kafka uses a simple TCP protocol to handle messaging communication.

### The Controller

In a kafka cluster, one broker is dynamically designated as the **Controller**. The controller coordinates the process of assigning partitions and data replicas to nodes in the cluster.

Every cluster has exactly one controller. If the controller goes down, another node will automatically become the controller.

## Partition & Replication

### Replication

[Replication](https://kafka.apache.org/documentation/#replication)

Kafka is designed with fault tolerance in mind. As a result, it includes build-in support for replication.

**Replication** means storing multiple copies of any given piece of data.
In Kafka, every topic is given a configurable replication factor.

The **replication factor** is the number of replicas tha t will be kept on different brokers for each partition in the topic.
![](captures\replicas.png "Replicas")

**Create topic & partition**

![](C:\Users\herve.yvon.f.zobo\apps\multi-skills\Kafka\captures\partition-creation.png "Partition Creation")

#### Leaders

In order to ensure that messages in a partition are kept in a consistent order across all replicas, Kafka chooses a **leader** for each partition.

The leader handles all reads and writes for the partition. The leader is dynamically selected and if the leader goes down, the cluster attempts to choose a new leader through a process called **leader election**.

### In-Sync Replicas

Kafka maintains a list of In-Sync Replicas (ISR) for each partition.

ISRs are replicas that are up-to-date with the leader. If a leader dies, the new leader is elected from amoung the ISRs.

By Default, if there are no remaining ISRs when a leader dies, Kafka waits until one becomes available. This means that producers will be on hold until a new leader can be elected.

You can turn on **Unclean leader election**, allowing the cluster to elect a non-in-sync replica in this scenario.

## The Life of a Message
[Topic & Logs](https://kafka.apache.org/documentation/#intro_topics)

![](captures\lifeofmessage.png "The Life of a message")

- **Producer** publishes a message to a **partition** within a **topic**.
- The message is added to the partition on the **leader**.
- The message is **copied to the replicas** of that partition on other brokers.
- **Consumers** read the message and process it.
- When the **retention period** for the message is reached, the message is deleted.


## TP - Create topic, producer & consumer

Create a Kafka Topic for the Inventory Purchase Data
Create the topic using the kafka-topics command:

`kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 3 --partitions 6 --topic inventory_purchases`

Note: After the creation command is ran, we may encounter a warning indicating that we should simply avoid creating another topic with a '.' called inventory.purchases.

Test the Setup by Publishing and Consuming Some Data
Start a command line producer:

`kafka-console-producer --broker-list localhost:9092 --topic inventory_purchases`

Develop a few lines of data that can be used for testing purposes. Since we are working with merely test data, a specific format is not required. It could look like this:
product: apples, quantity: 5
product: lemons, quantity: 7
Once the test images are published, we can exit the producer.

Start up a command line consumer:

`kafka-console-consumer --bootstrap-server localhost:9092 --topic inventory_purchases --from-beginning`

Note: The --from-beginning flag is used because we want to target our test messages.

We should see the test messages that were published earlier:
product: apples, quantity: 5
product: lemons, quantity: 7
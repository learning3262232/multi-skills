# The Kafka Java APIs

Kafka provides a series of **Application Programming Interfaces (APIs)**. These make it easier to write applications that use Kafka.

Kafka maintains a set of client libraries for **Java**, althrough there are open-source projects providing similar support for a variety of other languages.

Include these client libraries in your application to easily interact with Kafka.

- **Producer API**: Allows you to build producers that publish messages to Kafka.
- **Consumer API**: Allows you to build consumers that read Kafka messages.
- **Streams API**: Allows you to read from input topics, transform data, and output it to output topics.
- **Connect API**: Allows you to build custom connectors, which pull from or push to specific external systems.
- **AdminClient API**: Allows you to manage and inspect higher-level objects like topics and brokers.
# STREAMS


## What are Streams

**Kafka Streams** allows us to build applications that process Kafka data in real-time with ease.

A **Kafka Streams application** is an application where both the input and the output are stored in Kafka topics.

Kafk Streams is a client library (API) that makes it easy to build these applications.

## Kafka Streams Stateless Transformations

Kafka Streams provides a robust set of tools for processing and transforming data. The kafka cluster itself serves as the backend for data management and storage.

There are two types of data transformations in Kafka Streams:
- **Stateless Transformations** do not require any additional storage to manage the state.
- **Stateful transformations** require state store to manage the state.

### Stateless Transformations

- **Branch**: Splits a stream into multiple streams based on a predicate.
- **Filter**: Removes messages from the stream based on a condition.
- **FlatMap**: Takes input records and turns them into a different set of records.
- **Foreach**: Performs an arbitrary stateless operation on each record. This is a terminal operation and stops further processing.
- **GroupBy/GroupByKey**: Group records by their key. This is required to perform stateful transformations.
- **Map** : Allows you to read a record and produce a new, modified record.
- **Merge**: Merge two streams into one stream.
- **Peek**: Similar to *Foreach*, but does not stop processing.

## Kafka streams Aggregations -> Stateful

**Aggregations** are **stateful transformations** that always operate on these groups of records sharing the same key.

### Aggregations

- **Aggregate**: generates a new record from a calculation involving the grouped records.
- **Count**: Counts the number of records for each grouped key.
- **Reduce**: Combines the grouped records into a single record.

## Kafka Streams Joins

Joins are used to combine streams into one new stream.

- **Co-Partitioning**: When joining streams, the data must be co-partitioned:
  - Same number of partitions for input topic.
  - Same partitioning strategies for producers.

You can avoid the need for co-partitioning by using a **GlobalKTable**. With GlobalKTables, all instances of your streams application will populate the local table with data from **all partitions**.

- **Inner Join**: The new stream will contain only records that have a match in both joined streams.
- **Left Join**: The new stream will contain all records from the first stream, but only matching records from the joined stream.
- **Outer Join**: The new stream will contain all records from both streams.

## Kafka Streams Windowing

- **Windows** are similar to groups in that they deal with a set of records with the same key. However, windows further subdivide groups into *time buckets*.

- **Tumbling Time Windows**: Windows are based on time periods that never overlap or have gaps between them.

- **Hopping Time Windows**: Time-based, but can have overlaps or gaps between windows.
- **Sliding Time Windows**: These windows are dynamically based on the timestamps of records rather than a fixed point in time. They are only used in joins.
- **Session Windows**: Creates windows based on periods of activity. A group of records around the same timestamp will form a session window, whereas a period of **idle time** with no records in the group will not have a window.

### Late-Arriving Records

In real-world scenarios, it is always possible to receive out-of-order data.

When records fall into a time window received after the end of that window's grace pediod, they become known as **late-arriving records**.

You can specify a **retention period** for a window. Kafka Streams will retain old window buckets during this period so that late-arriving records can still be processed.

Any records that arrive after the retention period has expired will not be processed.

## Streams vs. Tables

Kafka Streams models data in two primary ways: **streams** and **tables**.

- **Streams**: Each record is a self-contained piece of data in an unbounded set of data. New records do not replace an existing piece of data with a new value.
- **Tables**: Records represent a current state that can be overwritten/updated.

Here are some example use cases:

**Stream**:
- Credit card transactions in real time
- A real-time log of attendees checking in to a conference.
- A log of customer purchases which represent the removal of items from a store's inventory.

**Table**:
- A user's current available credit card balance.
- A list of conference attendee names with a value indicating whether or not they have checked in.
- A set of data containing the quantity of each item in a store's inventory.

# kAFKA



## Setting up Kafka Servers

- Create a Kafka cluster
- we must address the configuration for some Servers

- Specifications:
	- 3 Servers
	- Distrition: Ubuntu 18.04 Bionic Beaver LTS
	- Size: 1 Large, 2 small

## Building a Kafka Cluster

The **Confluent Certified Developer for Apache kAFKA (CCDAK)** exam covers the Confluent version of Apache kAFKA.

Confluent is an enterprise platform built on Apache kAfka. Essentially, additional enterprise features.

We will be using and installing Confluent Community, a free version of Confluent. It includes all of the features of kAfka and Confluent covered on CCDAK exam.

## kAFKA Architecture Basics

### What is Apache Kafka

The Kafka documentation describes Apache as a *distributed streaming platform.*

This means that Kafka enables you to:

- Publish (write) and describes (read) to streams of data records (publier & decrire les flux de donnees)
- Store the records in a fault-tolerant and scalable fashion
- Process streams of records in real-time

#### Concepts and Terminology

- An **event** records the fact that "something happened" in the world or in your business.  It is also called record or message.
Conceptually, an event has a key, value, timestamp, and optional metadata headers.
- **Producers** are those client applications that publish (write) events to Kafka.
- **consumers** are those that subscribe to (read and process) these events
- a **topic** is similar to a folder in a filesystem, and the events are the files in that folder.
- **Topics are partitioned**, meaning a topic is spread over a number of "buckets" located on different Kafka brokers.
[Kafka introduction](https://kafka.apache.org/intro)

Some Background:
- Kafka is written in *Java*
- Originally created at LinkedIn
- Become Open source in 2011

#### Use Cases

- *Messaging*: **Building real-time streaming data pipelines that reliably get data between systems or applications**
note: ***Messaging** is one application talking to another*
exple: An online store application. When a customer makes a purchase, the application sends the order data to a backend that handles fulfillment/shipping.
You can use a Messaging platform like kafka to ensure thet this data is passed to the backend application reliably and with no data loss, even if the backend
application or one of your Kafka servers goes down.

- *Streaming*: Building real-time streaming applications that transform or react to the streams of data.
exple: Log aggregation< You have multiple servers creating log data. You can feed the data to Kafka and use streams to transform the data into a standardized format, then combine the log data from all of the servers into a single feed for analysis.

Note: ***Streaming is an application receiving data from Kafka, doing some kinds of processing and maybe event changing that data and feeling in the kafka***

#### Some Benefits of kAFKA

- Strong reliability guarantees
- Fault tolerance
- Robust APIs

### Kafka from the Command line

Kafka includes a series of shell scripts that can be used to interact with the Kafka cluster from command line.
These scripts can be found under the */bin* directory inside the main Kafka installation directory.
With a confluent package installation, the scripts are installed under /usr/bin and can be accessed as normal commands.

- :memo: [Kafka quickstart](https://kafka.apache.org/documentation/#quickstart)
- :memo: [Confluent-cli](https://docs.confluent.io/confluent-cli/current/overview.html)
- :memo: [Command line](./KafkaCommandLine.pdf)